const btnEl = document.getElementById("btn");
const animeContainerEl = document.querySelector(".anime-container");
const animeImgEl = document.querySelector(".anime-img");
const animeNameEl = document.querySelector(".anime-name");

btnEl.addEventListener("click", async function(){
    try {
        btnEl.disabled = true;
        btnEl.innerText = "Loading...";
        animeImgEl.src = "spinner.svg";
        animeNameEl.innerText = "Updating";
        const response = await fetch("https://api.catboys.com/img");
        const data = await response.json();
        btnEl.innerText = "Get Anime";
        btnEl.disabled = false;
        animeContainerEl.style.display = "block";
        animeImgEl.src = data.url;
        animeNameEl.innerText = data.artist;
    } catch (error) {
        btnEl.innerText = "Get Anime";
        btnEl.disabled = false;
        animeNameEl.innerText = "An error happened";
    }
})